package com.demo.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.ecommerce.dto.FeedbackDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.service.FeedBackService;


@RunWith(MockitoJUnitRunner.Silent.class)
public class FeedBackControllerTest {
	
	@Mock
	FeedBackService feedBackService;
	
	@InjectMocks
	FeedBackController feedBackController;
	
	@Test
	public void testSaveFeedback() {
		FeedbackDto feedbackDto = new FeedbackDto();
		ResponseDto responseDto = new ResponseDto();
		Mockito.when(feedBackService.saveFeedback(Mockito.any())).thenReturn(Optional.of(responseDto));
		ResponseEntity<Optional<ResponseDto>> response = feedBackController.saveFeedback(feedbackDto);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
}
