package com.demo.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.ecommerce.dto.ProductDTO;
import com.demo.ecommerce.service.ProductService;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductControllerTest {

	@Mock
	ProductService productService;
	
	@InjectMocks
	ProductsController productController;
	
	@Test
	public void testSearchProductByName() {
		List<ProductDTO> list = new ArrayList<>();
		Mockito.when(productService.findByProductSearch(Mockito.anyString().toUpperCase())).thenReturn(Optional.of(list));
		ResponseEntity<Optional<List<ProductDTO>>> response = productController.searchProductByName("HI");
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void testGetProductInfo() {
		ProductDTO productDto = new ProductDTO();
		Mockito.when(productService.getAllProductInfo(Mockito.anyLong())).thenReturn(Optional.of(productDto));
		ResponseEntity<Optional<ProductDTO>> response = productController.getProductInfoAll(1L);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
