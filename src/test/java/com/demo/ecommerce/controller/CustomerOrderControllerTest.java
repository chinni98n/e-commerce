package com.demo.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.ecommerce.dto.CustomerOrderResponseDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.service_impl.CustomerOrderServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerOrderControllerTest {
	@Mock
	CustomerOrderServiceImpl customerOrderService;

	@InjectMocks
	private CustomerOrderController customerOrderController;
	

	@Test
	public void testSaveOrder() {
		
		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("save success");
		responseDto.setStatusCode(200);
		Mockito.when(customerOrderService.saveCustomerOrder(Mockito.any())).thenReturn(Optional.of(responseDto));
		ResponseEntity<Optional<ResponseDto>> response = customerOrderController.buyProduct(Mockito.any());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void testShowOrders() {
		CustomerOrderResponseDto responseDto = new CustomerOrderResponseDto();
		/*
		 * responseDto.setBillAmount(200L); responseDto.setOrderId(1L);
		 * responseDto.setOrderTime(LocalDateTime.now());
		 */
		List<CustomerOrderResponseDto> list = new ArrayList<>();
		list.add(responseDto);
		Mockito.when(customerOrderService.showCustomerOders(Mockito.any())).thenReturn(Optional.of(list));
		ResponseEntity<Optional<List<CustomerOrderResponseDto>>> response = customerOrderController.showOrders(Mockito.any());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

}
