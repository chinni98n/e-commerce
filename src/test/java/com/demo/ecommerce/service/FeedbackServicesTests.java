package com.demo.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.ecommerce.appconstants.ApplicationConstants;
import com.demo.ecommerce.dto.FeedbackDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Feedback;
import com.demo.ecommerce.exception.InvalidRatingsException;
import com.demo.ecommerce.exception.InvalidUserException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.FeedbackRepository;
import com.demo.ecommerce.service_impl.FeedBackServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FeedbackServicesTests {

	@InjectMocks
	FeedBackServiceImpl feedBackServiceImpl;

	@Mock
	FeedbackRepository feedBackRepository;
	@Mock
	CustomerOrderRepository customerOrderRepository;

	@Test
	public void saveFeedback() {

		FeedbackDto feedbackDto = new FeedbackDto();
		feedbackDto.setCustomerId((long) 1);
		feedbackDto.setOrderId((long) 2);
		feedbackDto.setProductComments("nice");
		feedbackDto.setProductRating((long) 5);
		feedbackDto.setShopComments("super");
		feedbackDto.setShopRating((long) 5);
		Feedback feedback = new Feedback();
		feedback.setCustomerId((long) 1);
		feedback.setOrderId((long) 2);
		feedback.setProductComments("nice");
		feedback.setProductRating((long) 5);
		feedback.setShopComments("super");
		feedback.setShopRating((long) 5);
		List<CustomerOrder> list2 = new ArrayList<>();
		CustomerOrder customerOrder = new CustomerOrder();
		list2.add(customerOrder);

		Mockito.when(customerOrderRepository.findByCustomerIdAndOrderId(feedbackDto.getCustomerId(),
				feedbackDto.getOrderId())).thenReturn(Optional.of(list2));
		Mockito.when(feedBackRepository.save(feedback)).thenReturn(feedback);
		Optional<ResponseDto> responseDto = feedBackServiceImpl.saveFeedback(feedbackDto);
		ResponseDto responseDto2 = new ResponseDto();
		responseDto2.setStatusCode(ApplicationConstants.FEEDBACK_STATUS_CODE);
		responseDto2.setMessage(ApplicationConstants.FEEDBACK_STATUS_MESSAGE);
		assertEquals(responseDto.isPresent(), Optional.of(responseDto2).isPresent());
	}

	@Test(expected = InvalidUserException.class)
	public void saveFeedbackWithInvalidUserException() {

		FeedbackDto feedbackDto = new FeedbackDto();
		Feedback feedback = new Feedback();
		feedback.setCustomerId((long) 1);
		feedback.setOrderId((long) 2);
		feedback.setProductComments("nice");
		feedback.setProductRating((long) 5);
		feedback.setShopComments("super");
		feedback.setShopRating((long) 5);

		Mockito.when(feedBackRepository.save(feedback)).thenReturn(feedback);
		Optional<ResponseDto> response = feedBackServiceImpl.saveFeedback(feedbackDto);
		assertEquals(ApplicationConstants.FEEDBACK_STATUS_CODE, response.get().getStatusCode());
	}

	@Test(expected = InvalidRatingsException.class)
	public void saveFeedbackWithInvalidRating() {

		FeedbackDto feedbackDto = new FeedbackDto();
		feedbackDto.setCustomerId(1L);
		feedbackDto.setOrderId(2L);

		feedbackDto.setProductRating(6L);

		feedbackDto.setShopRating(6L);
		Feedback feedback = new Feedback();

		List<CustomerOrder> list2 = new ArrayList<>();
		CustomerOrder customerOrder = new CustomerOrder();
		list2.add(customerOrder);

		Mockito.when(customerOrderRepository.findByCustomerIdAndOrderId(feedbackDto.getCustomerId(),
				feedbackDto.getOrderId())).thenReturn(Optional.of(list2));
		Mockito.when(feedBackRepository.save(feedback)).thenReturn(feedback);
		feedBackServiceImpl.saveFeedback(feedbackDto);
	}

	@Test(expected = InvalidRatingsException.class)
	public void saveFeedbackWithInvalidRating2() {

		FeedbackDto feedbackDto = new FeedbackDto();
		feedbackDto.setCustomerId(1L);
		feedbackDto.setOrderId(2L);

		feedbackDto.setProductRating(6L);

		feedbackDto.setShopRating(6L);
		Feedback feedback = new Feedback();

		List<CustomerOrder> list2 = new ArrayList<>();
		CustomerOrder customerOrder = new CustomerOrder();
		list2.add(customerOrder);

		Mockito.when(customerOrderRepository.findByCustomerIdAndOrderId(feedbackDto.getCustomerId(),
				feedbackDto.getOrderId())).thenReturn(Optional.of(list2));
		Mockito.when(feedBackRepository.save(feedback)).thenReturn(feedback);
		feedBackServiceImpl.saveFeedback(feedbackDto);
	}
	
	@Test(expected = InvalidRatingsException.class)
	public void saveFeedbackWithInvalidRating3() {

		FeedbackDto feedbackDto = new FeedbackDto();
		feedbackDto.setCustomerId(1L);
		feedbackDto.setOrderId(2L);

		feedbackDto.setProductRating(6L);

		feedbackDto.setShopRating(2L);
		Feedback feedback = new Feedback();

		List<CustomerOrder> list2 = new ArrayList<>();
		CustomerOrder customerOrder = new CustomerOrder();
		list2.add(customerOrder);

		Mockito.when(customerOrderRepository.findByCustomerIdAndOrderId(feedbackDto.getCustomerId(),
				feedbackDto.getOrderId())).thenReturn(Optional.of(list2));
		Mockito.when(feedBackRepository.save(feedback)).thenReturn(feedback);
		feedBackServiceImpl.saveFeedback(feedbackDto);
	}

}
