package com.demo.ecommerce.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.ecommerce.appconstants.ApplicationConstants;
import com.demo.ecommerce.dto.BuyRequestDto;
import com.demo.ecommerce.dto.CustomerOrderResponseDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.entity.ShopQuantity;
import com.demo.ecommerce.exception.LessQuantityException;
import com.demo.ecommerce.exception.OrdersNotFoundException;
import com.demo.ecommerce.exception.OutOfStockException;
import com.demo.ecommerce.exception.RecordNotFoundException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.repository.ShopQuantityRepository;
import com.demo.ecommerce.service_impl.CustomerOrderServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerOrderServiceTest {

	@Mock
	CustomerOrderRepository customerOrderRepository;

	@Mock
	ShopQuantityRepository shopQuantityRepository;
	
	@Mock
	ProductRepository productRepository;

	@InjectMocks
	private CustomerOrderServiceImpl customerOrderService;

	@Test
	public void testSaveCustomerOrder() {
		ShopQuantity shopQuantity = new ShopQuantity();
		shopQuantity.setQuantity(2L);
		BuyRequestDto buyRequestDto = new BuyRequestDto();
		buyRequestDto.setProductId(1L);
		buyRequestDto.setQuantity(2L);
		buyRequestDto.setShopId(1L);

		Mockito.when(shopQuantityRepository.findByShopIdAndProductId(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(Optional.of(shopQuantity));

		Optional<ResponseDto> response = customerOrderService.saveCustomerOrder(buyRequestDto);
		assertEquals(ApplicationConstants.ORDER_SUCCESSFULL_CODE, response.get().getStatusCode());
	}

	@Test(expected = RecordNotFoundException.class)
	public void testSaveCustomerOrderNegative() {
		ShopQuantity shopQuantity = new ShopQuantity();
		shopQuantity.setQuantity(2L);

		BuyRequestDto buyRequestDto = new BuyRequestDto();
		buyRequestDto.setProductId(1L);
		buyRequestDto.setQuantity(2L);
		buyRequestDto.setShopId(1L);
		Mockito.when(shopQuantityRepository.findByShopIdAndProductId(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(Optional.ofNullable(null));

		Optional<ResponseDto> response = customerOrderService.saveCustomerOrder(buyRequestDto);
		assertEquals(ApplicationConstants.ORDER_SUCCESSFULL_CODE, response.get().getStatusCode());
	}

	@Test(expected = OutOfStockException.class)
	public void testSaveCustomerOrderNegative2() {
		BuyRequestDto buyRequestDto = new BuyRequestDto();
		buyRequestDto.setProductId(1L);
		buyRequestDto.setQuantity(2L);
		buyRequestDto.setShopId(1L);

		ShopQuantity shopQuantity = new ShopQuantity();
		shopQuantity.setQuantity(0L);
		Mockito.when(shopQuantityRepository.findByShopIdAndProductId(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(Optional.of(shopQuantity));

		Optional<ResponseDto> response = customerOrderService.saveCustomerOrder(buyRequestDto);
		assertEquals(ApplicationConstants.OUT_OF_STOCK_CODE, response.get().getStatusCode());
	}

	@Test(expected = LessQuantityException.class)
	public void testSaveCustomerOrderNegative3() {
		ShopQuantity shopQuantity = new ShopQuantity();
		shopQuantity.setQuantity(1L);
		BuyRequestDto buyRequestDto = new BuyRequestDto();
		buyRequestDto.setProductId(1L);
		buyRequestDto.setQuantity(2L);
		buyRequestDto.setShopId(1L);

		Mockito.when(shopQuantityRepository.findByShopIdAndProductId(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(Optional.of(shopQuantity));

		Optional<ResponseDto> response = customerOrderService.saveCustomerOrder(buyRequestDto);
		assertEquals(ApplicationConstants.ORDER_SUCCESSFULL_CODE, response.get().getStatusCode());
	}
	
	@Test
	public void testShowCustomerOrders() {
		Product product = new Product();
		product.setProductName("Men's fastrack watch");
		product.setPrice(1000L);
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setProduct_id(1L);
		customerOrder.setQuantity(2L);
		List<CustomerOrder> list = new ArrayList<>();
		list.add(customerOrder);
		Mockito.when(customerOrderRepository.findAllByCustomerId(Mockito.anyLong())).thenReturn(Optional.of(list));
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(product));
		Optional<List<CustomerOrderResponseDto>> response = customerOrderService.showCustomerOders(1L);
		assertNotNull(response);
	}
	
	@Test(expected = OrdersNotFoundException.class)
	public void testShowCustomerOrdersNegative() {
		List<CustomerOrder> list = null;
		Mockito.when(customerOrderRepository.findAllByCustomerId(Mockito.anyLong())).thenReturn(Optional.ofNullable(list));
		Optional<List<CustomerOrderResponseDto>> response = customerOrderService.showCustomerOders(1L);
		assertNotNull(response);
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testShowCustomerOrdersNegative2() {
		CustomerOrder customerOrder = new CustomerOrder();
		List<CustomerOrder> list = new ArrayList<>();
		list.add(customerOrder);
		Mockito.when(customerOrderRepository.findAllByCustomerId(Mockito.anyLong())).thenReturn(Optional.of(list));
		Optional<List<CustomerOrderResponseDto>> response = customerOrderService.showCustomerOders(1L);
		assertNotNull(response);
	}
}
