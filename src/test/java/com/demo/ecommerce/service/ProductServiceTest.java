package com.demo.ecommerce.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.ecommerce.dto.ProductDTO;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Feedback;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.exception.ProductNotFoundException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.FeedbackRepository;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.service_impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceTest {

	@InjectMocks
	ProductServiceImpl productServiceImpl;

	@Mock
	ProductRepository productRepository;

	@Mock
	CustomerOrderRepository customerOrderRepo;

	@Mock
	FeedbackRepository feedBackRepository;

	@Test
	public void productPostiveSearch() {

		String productName = "FRIDGE";

		List<Product> listOfProduct = new ArrayList<Product>();

		Product product = new Product();

		product.setBrand("MI");
		product.setDescription("jhsdjhjk");
		product.setProductName("FRIDGE");
		product.setPrice(8378l);
		listOfProduct.add(product);

		Mockito.when(productRepository.findByProductName(Mockito.any())).thenReturn(listOfProduct);

		Optional<List<ProductDTO>> list2 = productServiceImpl.findByProductSearch(productName);

		assertEquals(list2.get().size(), listOfProduct.size());

	}

	@Test(expected = ProductNotFoundException.class)
	public void productNegativeSearch() {

		String productName = "FRIDGE";

		List<Product> listOfProduct = new ArrayList<Product>();

		Mockito.when(productRepository.findByProductName(Mockito.any())).thenReturn(listOfProduct);

		productServiceImpl.findByProductSearch(productName);

	}

	@Test
	public void testGetAllProductInfo() {
		List<Product> listOfProduct = new ArrayList<Product>();

		Product product = new Product();

		product.setBrand("MI");
		product.setDescription("jhsdjhjk");
		product.setProductName("FRIDGE");
		product.setPrice(8378l);
		listOfProduct.add(product);

		List<CustomerOrder> listOfCustomerOrder = new ArrayList<>();
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setCustomerId((long) 1);
		customerOrder.setOrderId((long) 2);
		listOfCustomerOrder.add(customerOrder);
		Feedback feedback = new Feedback();
		feedback.setProductRating((long) 5);
		Mockito.when(feedBackRepository.findByOrderId((long) 2)).thenReturn(feedback);
		Mockito.when(productRepository.findById((long) 2)).thenReturn(Optional.of(product));
		Mockito.when(customerOrderRepo.findByProductId((long) 2)).thenReturn(listOfCustomerOrder);
		Optional<ProductDTO> productDTO = productServiceImpl.getAllProductInfo((long) 2);
		assertEquals("FRIDGE", productDTO.get().getProductName());
	}

	@Test(expected = ProductNotFoundException.class)
	public void getAllProductInfoWithProductNotFoundException() {
		Mockito.when(productRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		assertNotNull(productServiceImpl.getAllProductInfo(1L));
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetAllProductInfoNegative() {
		List<Product> listOfProduct = new ArrayList<Product>();

		Product product = new Product();

		product.setBrand("MI");
		product.setDescription("jhsdjhjk");
		product.setProductName("FRIDGE");
		product.setPrice(8378l);
		listOfProduct.add(product);

		List<CustomerOrder> listOfCustomerOrder = new ArrayList<>();
		Feedback feedback = new Feedback();
		feedback.setProductRating((long) 5);
		Mockito.when(feedBackRepository.findByOrderId((long) 2)).thenReturn(feedback);
		Mockito.when(productRepository.findById((long) 2)).thenReturn(Optional.of(product));
		Mockito.when(customerOrderRepo.findByProductId((long) 2)).thenReturn(listOfCustomerOrder);
		Optional<ProductDTO> productDTO = productServiceImpl.getAllProductInfo((long) 2);
		assertNotNull(productDTO);
	}
}
