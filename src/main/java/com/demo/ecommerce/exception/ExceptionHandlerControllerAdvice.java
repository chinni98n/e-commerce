package com.demo.ecommerce.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.demo.ecommerce.appconstants.ApplicationConstants;

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {
	@ExceptionHandler(InvalidRatingsException.class)
	public ExceptionResponse handleException(final InvalidRatingsException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.RATING_STATUS_CODE);
		return error;
	}

	@ExceptionHandler(InvalidUserException.class)
	public ExceptionResponse handleInvalidUserException(final InvalidUserException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.INVALID_USER_STATUS_CODE);
		return error;
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public ExceptionResponse handleException(final RecordNotFoundException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.RECORD_NOT_FOUND_CODE);
		return error;
	}

	@ExceptionHandler(LessQuantityException.class)
	public ExceptionResponse handleException(final LessQuantityException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.LESS_QUANTITY_CODE);
		return error;
	}

	@ExceptionHandler(OrdersNotFoundException.class)
	public ExceptionResponse handleException(final OrdersNotFoundException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.NO_ORDERS_CODE);
		return error;
	}

	@ExceptionHandler(ProductNotFoundException.class)
	public ExceptionResponse handlingFlightNotFoundException(final ProductNotFoundException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.STATUS_CODE);
		return error;
	}
}
