package com.demo.ecommerce.exception;

public class OutOfStockException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public OutOfStockException() {
		super();
	}

	public OutOfStockException(final String message) {
		super(message);
	}
}
