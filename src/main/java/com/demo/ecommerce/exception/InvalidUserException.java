package com.demo.ecommerce.exception;

public class InvalidUserException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InvalidUserException() {
		super();
	}

	public InvalidUserException(final String message) {
		super(message);
	}
}
