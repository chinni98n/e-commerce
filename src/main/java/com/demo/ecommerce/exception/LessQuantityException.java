package com.demo.ecommerce.exception;

public class LessQuantityException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public LessQuantityException() {
		super();
	}

	public LessQuantityException(final String message) {
		super(message);
	}
}
