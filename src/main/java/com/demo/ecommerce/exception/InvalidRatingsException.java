package com.demo.ecommerce.exception;

public class InvalidRatingsException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InvalidRatingsException() {
		super();
	}

	public InvalidRatingsException(final String message) {
		super(message);
	}
}
