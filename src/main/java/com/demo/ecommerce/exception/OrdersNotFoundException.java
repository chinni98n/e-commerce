package com.demo.ecommerce.exception;

public class OrdersNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public OrdersNotFoundException() {
		super();
	}

	public OrdersNotFoundException(final String message) {
		super(message);
	}
}
