package com.demo.ecommerce.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.dto.FeedbackDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.service.FeedBackService;

@RestController
@RequestMapping("/customer")
public class FeedBackController {

	@Autowired
	FeedBackService feedBackService;

	@PostMapping("/feedback")
	public ResponseEntity<Optional<ResponseDto>> saveFeedback(@RequestBody FeedbackDto feedbackDto) {
		Optional<ResponseDto> responseDto = feedBackService.saveFeedback(feedbackDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}
