package com.demo.ecommerce.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.dto.BuyRequestDto;
import com.demo.ecommerce.dto.CustomerOrderResponseDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.service_impl.CustomerOrderServiceImpl;

@RequestMapping("/customers")
@RestController
public class CustomerOrderController {

	@Autowired
	CustomerOrderServiceImpl customerOrderService;

	@PostMapping("/orders")
	public ResponseEntity<Optional<ResponseDto>> buyProduct(@RequestBody BuyRequestDto buyRequestDto) {
		Optional<ResponseDto> responseDto = customerOrderService.saveCustomerOrder(buyRequestDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@GetMapping("/{customerId}/orders")
	public ResponseEntity<Optional<List<CustomerOrderResponseDto>>> showOrders(
			@PathVariable("customerId") Long customerId) {
		Optional<List<CustomerOrderResponseDto>> customerOrderResponseDtoList = customerOrderService
				.showCustomerOders(customerId);
		return new ResponseEntity<>(customerOrderResponseDtoList, HttpStatus.OK);
	}
}
