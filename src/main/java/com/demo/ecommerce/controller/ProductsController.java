package com.demo.ecommerce.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.dto.ProductDTO;
import com.demo.ecommerce.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductsController {

	@Autowired
	ProductService productService;

	/**
	 * 
	 * @param productName : String
	 * 
	 *                    Here we are taking the productName from the user
	 * 
	 * @return List<Products>
	 * 
	 * @param productName  :String
	 * @param productPrice :String
	 * @param description  :String
	 * 
	 *                     Here we are return the list of searhed products
	 */

	@GetMapping("/search/{productName}")
	public ResponseEntity<Optional<List<ProductDTO>>> searchProductByName(@PathVariable("productName") String productName) {
		 Optional<List<ProductDTO>> response = productService.findByProductSearch(productName.toUpperCase());
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * 
	 * @param productId : Long
	 * 
	 *                  Here we are taking the productId from customer to find
	 *                  product details
	 * 
	 * @return ProductDto
	 * 
	 * @param productName  :String
	 * @param productPrice :String
	 * @param description  :String
	 * @param ratings      :Long Average
	 * @param availability : status of the product
	 * 
	 *                     Here we are return entire product information with
	 *                     ratings
	 */

	@GetMapping("/products/{productId}")
	public ResponseEntity<Optional<ProductDTO>> getProductInfoAll(@PathVariable("productId") Long productId) {
		 Optional<ProductDTO> response = productService.getAllProductInfo(productId);
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
