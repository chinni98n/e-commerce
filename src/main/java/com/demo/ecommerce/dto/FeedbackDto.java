package com.demo.ecommerce.dto;

public class FeedbackDto {

	private Long customerId;
	private Long orderId;
	private Long productRating;
	private String productComments;
	private Long shopRating;
	private String shopComments;
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Long getProductRating() {
		return productRating;
	}
	public void setProductRating(Long productRating) {
		this.productRating = productRating;
	}
	public String getProductComments() {
		return productComments;
	}
	public void setProductComments(String productComments) {
		this.productComments = productComments;
	}
	public Long getShopRating() {
		return shopRating;
	}
	public void setShopRating(Long shopRating) {
		this.shopRating = shopRating;
	}
	public String getShopComments() {
		return shopComments;
	}
	public void setShopComments(String shopComments) {
		this.shopComments = shopComments;
	}
	
	
	
}
