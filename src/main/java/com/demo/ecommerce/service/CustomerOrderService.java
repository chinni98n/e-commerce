package com.demo.ecommerce.service;

import java.util.List;
import java.util.Optional;

import com.demo.ecommerce.dto.BuyRequestDto;
import com.demo.ecommerce.dto.CustomerOrderResponseDto;
import com.demo.ecommerce.dto.ResponseDto;

public interface CustomerOrderService {

	Optional<ResponseDto> saveCustomerOrder(BuyRequestDto buyRequestDto); 
	
	Optional<List<CustomerOrderResponseDto>> showCustomerOders(Long customerId);
	
}
