package com.demo.ecommerce.service;

import java.util.Optional;

import com.demo.ecommerce.dto.FeedbackDto;
import com.demo.ecommerce.dto.ResponseDto;


public interface FeedBackService {

	Optional<ResponseDto> saveFeedback(FeedbackDto feedbackDto);

}
