package com.demo.ecommerce.service;


import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.demo.ecommerce.dto.ProductDTO;


@Service
public interface ProductService {

	public Optional<List<ProductDTO>> findByProductSearch(String productName);
	
	public Optional<ProductDTO> getAllProductInfo(Long productId);

}
