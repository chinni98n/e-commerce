package com.demo.ecommerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "feedback")
@Entity
public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long customerId;
	private Long orderId;
	private Long productRating;
	private String productComments;
	private Long shopRating;
	private String shopComments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getProductRating() {
		return productRating;
	}

	public void setProductRating(Long productRating) {
		this.productRating = productRating;
	}

	public String getProductComments() {
		return productComments;
	}

	public void setProductComments(String productComments) {
		this.productComments = productComments;
	}

	public Long getShopRating() {
		return shopRating;
	}

	public void setShopRating(Long shopRating) {
		this.shopRating = shopRating;
	}

	public String getShopComments() {
		return shopComments;
	}

	public void setShopComments(String shopComments) {
		this.shopComments = shopComments;
	}

}
