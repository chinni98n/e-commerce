package com.demo.ecommerce.service_impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ecommerce.appconstants.ApplicationConstants;
import com.demo.ecommerce.dto.ProductDTO;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Feedback;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.exception.ProductNotFoundException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.FeedbackRepository;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	CustomerOrderRepository customerOrderRepo;

	@Autowired
	FeedbackRepository feedBackRepository;

	@Override
	public Optional<List<ProductDTO>> findByProductSearch(String productName) {

		List<Product> productsInfo = productRepository.findByProductName(productName);

		List<ProductDTO> listOfProducts = new ArrayList<>();

		for (Product product : productsInfo) {

			ProductDTO productDto = new ProductDTO();

			productDto.setProductId(product.getProductId());
			productDto.setProductName(product.getProductName());
			productDto.setBrand(product.getBrand());
			productDto.setDescription(product.getDescription());
			productDto.setProductPrice(product.getPrice());

			listOfProducts.add(productDto);

		}
		if (listOfProducts.isEmpty()) {

			throw new ProductNotFoundException(ApplicationConstants.STATUS_MESSAGE);

		}

		return Optional.of(listOfProducts);
	}

	@Override
	public Optional<ProductDTO> getAllProductInfo(Long productId) {

		Optional<Product> productInfo = productRepository.findById(productId);

		if (!productInfo.isPresent()) {
			throw new ProductNotFoundException(ApplicationConstants.STATUS_MESSAGE);
		}
		ProductDTO productDto = new ProductDTO();

		productDto.setProductId(productInfo.get().getProductId());
		productDto.setBrand(productInfo.get().getBrand());
		productDto.setDescription(productInfo.get().getDescription());
		productDto.setProductName(productInfo.get().getProductName());
		productDto.setProductPrice(productInfo.get().getPrice());

		List<CustomerOrder> listOfCustomerOrders = customerOrderRepo.findByProductId(productId);

		long loopCount = 0;
		long rating = 0;
		for (CustomerOrder customerInfo : listOfCustomerOrders) {

			Long orderId = customerInfo.getOrderId();

			Feedback feedbackInfo = feedBackRepository.findByOrderId(orderId);

			rating = rating + feedbackInfo.getProductRating();

			loopCount++;

		}
		
		if(loopCount==0) {
			throw new NumberFormatException(ApplicationConstants.NUMBER_FORMAT_EXCEPTION_MESSAGE);
		}

		long averageRating = rating / loopCount;
		productDto.setRatings(averageRating);

		return Optional.of(productDto);
	}

}
