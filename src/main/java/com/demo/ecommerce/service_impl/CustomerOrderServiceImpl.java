package com.demo.ecommerce.service_impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ecommerce.appconstants.ApplicationConstants;
import com.demo.ecommerce.dto.BuyRequestDto;
import com.demo.ecommerce.dto.CustomerOrderResponseDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.entity.ShopQuantity;
import com.demo.ecommerce.exception.LessQuantityException;
import com.demo.ecommerce.exception.OrdersNotFoundException;
import com.demo.ecommerce.exception.OutOfStockException;
import com.demo.ecommerce.exception.RecordNotFoundException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.repository.ShopQuantityRepository;
import com.demo.ecommerce.service.CustomerOrderService;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

	@Autowired
	CustomerOrderRepository customerOrderRepository;
	@Autowired
	ShopQuantityRepository shopQuantityRepository;
	@Autowired
	ProductRepository productRepository;

	ModelMapper mapper = new ModelMapper();

	@Override
	public Optional<ResponseDto> saveCustomerOrder(BuyRequestDto buyRequestDto) {
		Long shopId = buyRequestDto.getShopId();
		Long productId = buyRequestDto.getProductId();
		Long requiredQuantity = buyRequestDto.getQuantity();

		Optional<ShopQuantity> shopQuantityOpt = shopQuantityRepository.findByShopIdAndProductId(shopId, productId);
		if (!shopQuantityOpt.isPresent())
			throw new RecordNotFoundException(ApplicationConstants.RECORD_NOT_FOUND);
		ShopQuantity shopQuantity = shopQuantityOpt.get();
		Long availableQuantity = shopQuantity.getQuantity();
		if (availableQuantity == 0)
			throw new OutOfStockException(ApplicationConstants.OUT_OF_STOCK);
		if (requiredQuantity > availableQuantity) {
			throw new LessQuantityException(ApplicationConstants.LESS_QUANTITY);
		}
		Long remainingQuantity = availableQuantity - requiredQuantity;
		shopQuantity.setQuantity(remainingQuantity);
		CustomerOrder customerOrder = mapper.map(buyRequestDto, CustomerOrder.class);
		customerOrder.setCustomerId(buyRequestDto.getCustomerId());
		customerOrder.setProduct_id(buyRequestDto.getProductId());
		customerOrderRepository.save(customerOrder);
		shopQuantityRepository.save(shopQuantity);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatusCode(ApplicationConstants.ORDER_SUCCESSFULL_CODE);
		responseDto.setMessage(ApplicationConstants.ORDER_SUCCESSFULL);
		return Optional.of(responseDto);
	}

	@Override
	public Optional<List<CustomerOrderResponseDto>> showCustomerOders(Long customerId) {
		Optional<List<CustomerOrder>> customerOrderListOpt = customerOrderRepository.findAllByCustomerId(customerId);
		if (!customerOrderListOpt.isPresent())
			throw new OrdersNotFoundException(ApplicationConstants.NO_ORDERS);
		List<CustomerOrder> customerOrderList = customerOrderListOpt.get();
		List<CustomerOrderResponseDto> customerOrderResponseDtoList = new ArrayList<>();
		for (CustomerOrder customerOrder : customerOrderList) {
			Long productId = customerOrder.getProductId();
			Optional<Product> productOpt = productRepository.findById(productId);
			if (!productOpt.isPresent())
				throw new RecordNotFoundException(ApplicationConstants.RECORD_NOT_FOUND);
			String productName = productOpt.get().getProductName();
			Long productPrice = productOpt.get().getPrice();
			Long amount = productPrice * customerOrder.getQuantity();
			CustomerOrderResponseDto customerOrderResponseDto =  mapper.map(customerOrder, CustomerOrderResponseDto.class);
			customerOrderResponseDto.setProductName(productName);
			customerOrderResponseDto.setBillAmount(amount);
			customerOrderResponseDtoList.add(customerOrderResponseDto);
		}
		return Optional.of(customerOrderResponseDtoList);
	}

}
