package com.demo.ecommerce.service_impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ecommerce.appconstants.ApplicationConstants;
import com.demo.ecommerce.dto.FeedbackDto;
import com.demo.ecommerce.dto.ResponseDto;
import com.demo.ecommerce.entity.CustomerOrder;
import com.demo.ecommerce.entity.Feedback;
import com.demo.ecommerce.exception.InvalidRatingsException;
import com.demo.ecommerce.exception.InvalidUserException;
import com.demo.ecommerce.repository.CustomerOrderRepository;
import com.demo.ecommerce.repository.FeedbackRepository;
import com.demo.ecommerce.service.FeedBackService;

@Service
public class FeedBackServiceImpl implements FeedBackService {

	@Autowired
	FeedbackRepository feedBackRepository;
	@Autowired
	CustomerOrderRepository customerOrderRepository;

	public Optional<ResponseDto> saveFeedback(FeedbackDto feedbackDto) {

		Optional<List<CustomerOrder>> list = customerOrderRepository
				.findByCustomerIdAndOrderId(feedbackDto.getCustomerId(), feedbackDto.getOrderId());
		if (!list.isPresent()) {
			throw new InvalidUserException(ApplicationConstants.INVALID_USER_STATUS_MESSAGE);
		}

		if (feedbackDto.getProductRating() > 5 || feedbackDto.getShopRating() > 5) {
			throw new InvalidRatingsException(ApplicationConstants.RATING_STATUS_MESSAGE);
		}
		Feedback feedback = new Feedback();
		feedback.setCustomerId(feedbackDto.getCustomerId());
		feedback.setOrderId(feedbackDto.getOrderId());
		feedback.setProductComments(feedbackDto.getProductComments());
		feedback.setProductRating(feedbackDto.getProductRating());
		feedback.setShopComments(feedbackDto.getProductComments());
		feedback.setShopRating(feedbackDto.getShopRating());
		feedBackRepository.save(feedback);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatusCode(ApplicationConstants.FEEDBACK_STATUS_CODE);
		responseDto.setMessage(ApplicationConstants.FEEDBACK_STATUS_MESSAGE);
		return Optional.of(responseDto);
	}

}
