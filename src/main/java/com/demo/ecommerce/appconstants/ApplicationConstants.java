package com.demo.ecommerce.appconstants;

public class ApplicationConstants {

	private ApplicationConstants() {

	}

	public static final Integer ORDER_SUCCESSFULL_CODE = 601;
	public static final String ORDER_SUCCESSFULL = "Your order placed successfully";
	public static final Integer RECORD_NOT_FOUND_CODE = 602;
	public static final String RECORD_NOT_FOUND = "Record not found";
	public static final Integer LESS_QUANTITY_CODE = 602;
	public static final String LESS_QUANTITY = "Requested number of products are not available";
	public static final Integer OUT_OF_STOCK_CODE = 603;
	public static final String OUT_OF_STOCK = "OUT OF STOCK";
	public static final Integer NO_ORDERS_CODE = 604;
	public static final String NO_ORDERS = "No orders found";

	public static final Integer STATUS_CODE = 904;
	public static final String STATUS_MESSAGE = "we didn't found any products on your search,please try agin";

	// public static final Integer STATUS_CODE = 909;
//	public static final String STATUS_MESSAGE = "";

	public static final Integer FEEDBACK_STATUS_CODE = 901;
	public static final String FEEDBACK_STATUS_MESSAGE = "Feedback Submitted successfully";

	public static final Integer RATING_STATUS_CODE = 902;
	public static final String RATING_STATUS_MESSAGE = "Rating should be up to 5";

	public static final Integer INVALID_USER_STATUS_CODE = 903;
	public static final String INVALID_USER_STATUS_MESSAGE = "Invalid User for submitting feedback";
	
	public static final Integer NUMBER_FORMAT_EXCEPTION_CODE = 904;
	public static final String NUMBER_FORMAT_EXCEPTION_MESSAGE = "Number Format Exception";
	
}
