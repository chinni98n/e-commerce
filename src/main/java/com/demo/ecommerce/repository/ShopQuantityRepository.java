package com.demo.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.ShopQuantity;

@Repository
public interface ShopQuantityRepository extends JpaRepository<ShopQuantity, Long>{

	Optional<ShopQuantity> findByShopIdAndProductId(Long shopId, Long productId);
}
