package com.demo.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.CustomerOrder;

@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {

	Optional<List<CustomerOrder>> findAllByCustomerId(Long customerId);

	List<CustomerOrder> findByProductId(Long productId);

	Optional<List<CustomerOrder>> findByCustomerIdAndOrderId(Long customerId, Long orderId);

}
