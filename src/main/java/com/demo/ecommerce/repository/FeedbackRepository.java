package com.demo.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.demo.ecommerce.entity.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

	com.demo.ecommerce.entity.Feedback findByOrderId(Long orderId);
}
